package com.example.mylibrary

import android.text.TextUtils

fun String.isValidEmail(): Boolean{
    return !TextUtils.isEmpty(this) && android.util.Patterns.EMAIL_ADDRESS.matcher(this).matches()
}

fun String.isTextLengthLessThan(length: Int = 8): Boolean{
    return this.length < length
}