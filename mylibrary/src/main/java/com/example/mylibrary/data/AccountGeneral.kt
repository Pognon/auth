package com.example.mylibrary.data

object AccountGeneral {
    /**
     * Account type id
     */
    const val ACCOUNT_TYPE = "com.echelon.dagga_storage"

    /**
     * Account name
     */
    const val ACCOUNT_NAME = "Echelon"

    /**
     * Auth token types
     */
    const val AUTHTOKEN_TYPE_READ_ONLY = "Read only"
    const val AUTHTOKEN_TYPE_READ_ONLY_LABEL = "Read only access to an Udinic account"
    const val AUTHTOKEN_TYPE_FULL_ACCESS = "Full access"
    const val AUTHTOKEN_TYPE_FULL_ACCESS_LABEL = "Full access to an Udinic account"
}