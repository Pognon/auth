package com.example.mylibrary.data.model.api

import android.util.Log
import com.example.mylibrary.data.PreferenceHelper
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class AuthInterceptor @Inject constructor(val preferenceHelper: PreferenceHelper): Interceptor {


    /* @Inject
     lateinit var preferenceHelper: PreferenceHelper*/

    override fun intercept(chain: Interceptor.Chain): Response {


        //val org = preferenceHelper.getOrganisation()
        Log.e("INTERCEPT ", "TOKEN " + preferenceHelper.getAccessToken())
        /*Log.e("INTERCEPT ", "ORG " + org?.id + " NAME " + org?.name)
        Log.e("INTERCEPT ", "FCM " + preferenceHelper.getFCMKey())
        Log.e("INTERCEPT ", "DEVICE ID " + preferenceHelper.getDeviceId())
        Log.e("INTERCEPT ", "DEVICE MODEL " + preferenceHelper.getDeviceModel())
        Log.e("INTERCEPT ", "DEVICE OS VERSION " + preferenceHelper.getOSVersion())
        Log.e("INTERCEPT ", "APP VERSION " + preferenceHelper.getAppVersion())*/

        val chain = if (preferenceHelper.isAuthenticated()/* && org?.id != null*/) {

            chain.proceed(
                chain.request().newBuilder()
                    .addHeader("Accept", "application/json")
                    .addHeader("app-source", "agrosurvey")
                    .addHeader("Authorization", "Bearer " + preferenceHelper.getAccessToken())
                    /*.addHeader("Organization-Id", org.id)
                    .addHeader("Device-uid", preferenceHelper.getDeviceId())
                    .addHeader("Device-Model", preferenceHelper.getDeviceModel())
                    .addHeader("APP-VERSION", preferenceHelper.getAppVersion())
                    .addHeader("OS-VERSION", preferenceHelper.getOSVersion())
                    .addHeader("FCM-Token", preferenceHelper.getFCMKey())*/
                    .build()
            )
        } else {
            chain.proceed(chain.request())
        }

        //val content = chain.peekBody(88000)?.string() //TODO
        //Log.e("INTERCEPT", "==  $content")
        return chain

    }
}