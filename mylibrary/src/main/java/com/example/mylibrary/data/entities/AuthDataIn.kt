package com.example.mylibrary.data.entities

data class AuthDataIn (
    val token_type: String? = null,
    val expires_in: Int? = null,
    val access_token: String? = null,
    val refresh_token: String? = null
)