package com.example.mylibrary.data

import com.example.mylibrary.data.entities.AuthDataError
import com.example.mylibrary.data.entities.AuthDataIn
import com.example.mylibrary.data.entities.UserOut
import retrofit2.http.Body
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part

interface AuthenticationService {
    @POST("signup/")
    suspend fun signUp(@Body user: UserOut): NetworkResponse<AuthDataIn, AuthDataError>

    @Multipart
    @POST("oauth/token")
    suspend fun authenticate(
        @Part("username") username: String? = null,
        @Part("password") password: String? = null,
        @Part("client_id") clientId: String? = "7",
        @Part("client_secret") clientSecret: String? = "oWWw8LQlu2VYibthgKpNTUSdV1CwxoxTLhFWYHCC",
        @Part("scope") scope: String? = "read_files",
        @Part("grant_type") grantType: String? = "password",
    ) : NetworkResponse<AuthDataIn, AuthDataError>
}