package com.example.mylibrary.data.entities

class AuthDataError (
    var type: String,
    var message: String? = null
)