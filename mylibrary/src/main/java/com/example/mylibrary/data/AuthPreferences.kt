package com.example.mylibrary.data

data class AuthPreferences(val isFirstTimeLaunch: Boolean, val token: String, val lSalt: String, val lIv: String)
