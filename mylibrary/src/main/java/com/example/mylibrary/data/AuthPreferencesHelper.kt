package com.example.mylibrary.data

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import java.io.IOException
import javax.inject.Inject

class AuthPreferencesHelper @Inject constructor(
    private val authPreferencesStore: DataStore<Preferences>
) {

    private object PreferencesKeys {
        val IS_FIRST_TIME_LAUNCH = booleanPreferencesKey("is_first_time_launch")
        val TOKEN = stringPreferencesKey("token")
        val LSALT = stringPreferencesKey("lsalt")
        val LIV = stringPreferencesKey("liv")
    }

    fun isFirstTimeLaunch(): Flow<Boolean> {
        return authPreferencesStore.data.catch { exception ->
            if(exception is IOException){
                emit(emptyPreferences())
            }else{
                throw exception
            }
        }.map { preferences ->
            mapAuthPreferences(preferences).isFirstTimeLaunch
        }
    }

    suspend fun updateFirstTimeLaunch(isFirstTime: Boolean) {
        authPreferencesStore.edit { preferences ->
            preferences[PreferencesKeys.IS_FIRST_TIME_LAUNCH] = isFirstTime
        }
    }

    private fun mapAuthPreferences(preferences: Preferences): AuthPreferences {
        val isFirstTime = preferences[PreferencesKeys.IS_FIRST_TIME_LAUNCH] ?: true
        val token = preferences[PreferencesKeys.TOKEN] ?: "n/a"
        val lsalt = preferences[PreferencesKeys.LSALT] ?: "n/a"
        val liv = preferences[PreferencesKeys.LIV] ?: "n/a"
        return AuthPreferences(isFirstTime, token, lsalt, liv)
    }

    suspend fun updateToken(token: String){
        authPreferencesStore.edit { preferences ->
            preferences[PreferencesKeys.TOKEN] = token
        }
    }

    fun getToken(): Flow<String> {
        return authPreferencesStore.data.map { preferences ->
            preferences[PreferencesKeys.TOKEN] ?: ""
        }
    }
    suspend fun updateLSalt(lsalt: String){
        authPreferencesStore.edit { preferences ->
            preferences[PreferencesKeys.LSALT] = lsalt
        }
    }

    fun getLSalt(): Flow<String> {
        return authPreferencesStore.data.map { preferences ->
            preferences[PreferencesKeys.LSALT] ?: ""
        }
    }

    suspend fun updateLIv(liv: String){
        authPreferencesStore.edit { preferences ->
            preferences[PreferencesKeys.LIV] = liv
        }
    }

    fun getLIv(): Flow<String> {
        return authPreferencesStore.data.map { preferences ->
            preferences[PreferencesKeys.LIV] ?: ""
        }
    }

}