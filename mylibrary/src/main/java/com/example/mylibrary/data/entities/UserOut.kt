package com.example.mylibrary.data.entities
data class UserOut(
    val email: String,
    val password: String
)
