package com.example.mylibrary.data

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import javax.inject.Inject

class PreferenceHelper @Inject constructor(context: Context) {

    companion object {
        private const val SP_NAME = "com.gabriel.ahava.sp"

        private const val AUTH_STATUS_KEY = SP_NAME + "auth_status"

        private const val ACCESS_TOKEN_KEY = SP_NAME + "access_token"

        private const val LAST_CONNECTION = SP_NAME + "LAST"

        private const val SHOW_LINE_NUMBERS_KEY = SP_NAME + "show_line_numbers"

        private const val USER_LOGIN_KEY = SP_NAME + "user_login"

        private const val STATS = SP_NAME + "stats"

        private const val CURRENT_USER_TOKEN = SP_NAME + "current_user_token"

        private const val CURRENT_USER_PROFILE = SP_NAME + "current_user_profile"

        private const val CURRENT_USER_KEY = SP_NAME + "current_user_key"

        private const val NONE_USER_LOGIN = "None"

        const val EMPTY_ACCESS_TOKEN = "empty"

        const val FIRST_TIME_LAUNCH = "fist_time_launnch"
    }


    private val sp: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)


    fun getAuthStatus(): Boolean = sp.getBoolean(AUTH_STATUS_KEY, false)

    fun setIsAuthenticated(status: Boolean) {
        sp.edit().putBoolean(AUTH_STATUS_KEY, status).apply()
    }

    fun isAuthenticated(): Boolean = sp.getBoolean(AUTH_STATUS_KEY, false)


    fun setCurrentUserLogin(login: String) {
        sp.edit().putString(USER_LOGIN_KEY, login).apply()
    }

    fun getAccessToken(): String = sp.getString(ACCESS_TOKEN_KEY, EMPTY_ACCESS_TOKEN).toString()

    fun getLastConnection(): String = sp.getString(LAST_CONNECTION, NONE_USER_LOGIN).toString()

    fun getCurrentUserProfile(): String =
        sp.getString(CURRENT_USER_PROFILE, NONE_USER_LOGIN).toString()

    fun setAccessToken(token: String) {
        sp.edit().putString(ACCESS_TOKEN_KEY, token).apply()

    }

    fun setCurrentUserToken(tokens: String) {
        sp.edit().putString(CURRENT_USER_TOKEN, tokens).apply()

    }

    fun setLastConnection(lastConnection: String) {
        sp.edit().putString(LAST_CONNECTION, lastConnection).apply()
    }

    fun isFirstimeLaunch(): Boolean = sp.getBoolean(FIRST_TIME_LAUNCH, true)
    fun setFirstTimeLaunch(value: Boolean) = sp.edit().putBoolean(FIRST_TIME_LAUNCH, value).apply()

    fun setCurrentUserProfile(profile: String) {
        sp.edit().putString(CURRENT_USER_PROFILE, profile).apply()
    }
}

