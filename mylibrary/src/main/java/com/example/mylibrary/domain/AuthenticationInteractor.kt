package com.example.mylibrary.domain

import com.example.mylibrary.data.AuthenticationService
import com.example.mylibrary.data.entities.UserOut
import javax.inject.Inject

class AuthenticationInteractor @Inject constructor(val authenticationService: AuthenticationService){

    suspend fun authenticateUser(email: String, password:String) = authenticationService.authenticate(username= email, password = password)

    suspend fun signUser(userOut: UserOut) = authenticationService.signUp(userOut)
}