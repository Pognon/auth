package com.example.mylibrary

import android.app.Service
import android.content.Intent
import android.os.IBinder

class DaggaAuthenticatorService : Service() {
    override fun onBind(intent: Intent?): IBinder {
        val authenticator = DaggaAuthenticator(this)
        return authenticator.iBinder
    }
}