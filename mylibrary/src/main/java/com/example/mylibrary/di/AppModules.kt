package com.example.mylibrary.di

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.preferencesDataStore
import com.example.mylibrary.data.AuthPreferencesHelper
import com.example.mylibrary.data.PreferenceHelper
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Qualifier

val Context.dataStore by preferencesDataStore("DAGGA_PREFERENCE")

@Module
@InstallIn(SingletonComponent::class)
object AppModules {

    @Provides
    fun providesPreferenceHelper(@ApplicationContext context: Context): PreferenceHelper {
        return PreferenceHelper(context)
    }

    @Qualifier
    @Retention(AnnotationRetention.BINARY)
    annotation class DaggaPreferencesDataStore

    @DaggaPreferencesDataStore
    @Provides
    fun providePreferenceDataStore(@ApplicationContext context: Context): DataStore<Preferences> {
        return context.dataStore
    }

    @Provides
    fun providesAuthPreferenceHelper(@DaggaPreferencesDataStore dataStore: DataStore<Preferences>): AuthPreferencesHelper {
        return AuthPreferencesHelper(dataStore)
    }
}