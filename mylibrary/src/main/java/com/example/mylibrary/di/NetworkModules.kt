package com.example.mylibrary.di

import android.util.Log
import com.example.mylibrary.Constants
import com.example.mylibrary.data.AuthenticationService
import com.example.mylibrary.data.NetworkResponseAdapterFactory
import com.example.mylibrary.data.PreferenceHelper
import com.example.mylibrary.data.StringConverterFactory
import com.example.mylibrary.data.model.api.AuthInterceptor
import com.example.mylibrary.domain.AuthenticationInteractor
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Qualifier

@Module
@InstallIn(SingletonComponent::class)
object NetworkModules {
    @Provides
    fun providesAuthInterceptor(preferenceHelper: PreferenceHelper): AuthInterceptor {
        return AuthInterceptor(preferenceHelper)
    }

    @Qualifier
    @Retention(AnnotationRetention.BINARY)
    annotation class DaggaAuthOkHttpClient

    @DaggaAuthOkHttpClient
    @Provides
    fun providesOkHttpClient(authenInterceptor: AuthInterceptor): OkHttpClient {

        val interceptor = HttpLoggingInterceptor { message -> Log.e("REQUEST", message) }
        interceptor.level = HttpLoggingInterceptor.Level.BASIC

        return OkHttpClient.Builder()
            .addInterceptor(authenInterceptor)
            .addInterceptor(interceptor)
            /*.addInterceptor { chain ->
                Log.e("NEW REQUESTchain.request().url
            }*/
            .writeTimeout(3, TimeUnit.MINUTES)
            .readTimeout(3, TimeUnit.MINUTES)
            .retryOnConnectionFailure(true)
            .build()
    }

    @Qualifier
    @Retention(AnnotationRetention.BINARY)
    annotation class RetrofitWithAuthInterceptor

    @Qualifier
    @Retention(AnnotationRetention.BINARY)
    annotation class RetrofitWithoutAuthInterceptor

    @RetrofitWithAuthInterceptor
    @Provides
    fun providesRetrofitWithAuthInterceptor(@DaggaAuthOkHttpClient httpClient: OkHttpClient): Retrofit {

        val moshi = Moshi.Builder()
            .addLast(KotlinJsonAdapterFactory())
            .build()
        return Retrofit.Builder()
            .baseUrl(Constants.API_URL)//TODO inject this
            .addCallAdapterFactory(NetworkResponseAdapterFactory())
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .client(httpClient)
            .build()
    }

    @RetrofitWithoutAuthInterceptor
    @Provides
    fun providesRetrofitWithoutAuthInterceptor(@DaggaAuthOkHttpClient httpClient: OkHttpClient): Retrofit {

        val moshi = Moshi.Builder()
            .addLast(KotlinJsonAdapterFactory())
            .build()
        return Retrofit.Builder()
            .baseUrl(Constants.API_URL)//TODO inject this
            .addConverterFactory(StringConverterFactory.create())
            .addCallAdapterFactory(NetworkResponseAdapterFactory())
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .client(httpClient)
            .build()
    }

    @Provides
    fun providesAuthenticationService(@RetrofitWithoutAuthInterceptor retrofit: Retrofit): AuthenticationService{
        return retrofit.create(AuthenticationService::class.java)
    }

    @Provides
    fun providesAuthenticationInteractor(authentificationService: AuthenticationService): AuthenticationInteractor {
        return AuthenticationInteractor(authentificationService)
    }
}