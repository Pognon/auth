package com.example.mylibrary

import android.accounts.AccountManager
import android.accounts.AccountManagerFuture
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.widget.Toast
import androidx.core.content.ContextCompat.startActivity

class DaggAuth {

    fun hello(name: String = "World"): String{
        return "Hello $name"
    }

    fun setActivity(activity: Class<*>){
        Constants.activity = activity
    }

    fun getTokenForAccountCreateIfNeeded(activity: Activity, mAccountManager: AccountManager? = null, accountType: String, authTokenType: String) {
        val future: AccountManagerFuture<Bundle> = mAccountManager!!.getAuthTokenByFeatures(
            accountType,
            authTokenType,
            null,
            activity,
            null,
            null,
            { future ->
                var bnd: Bundle? = null
                try {
                    bnd = future.result
                    val authtoken = bnd?.getString(AccountManager.KEY_AUTHTOKEN)
                    val intent = Intent(activity, Constants.activity)
                    activity.startActivity(intent)
                    //showMessage(if (authtoken != null) "SUCCESS!\ntoken: $authtoken" else "FAIL")
                    Log.d("Dagga", "GetTokenForAccount Bundle is $bnd")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            },
            null)

    }
    /*private fun showMessage(msg: String) {
        if (TextUtils.isEmpty(msg)) return
        runOnUiThread { Toast.makeText(baseContext, msg, Toast.LENGTH_SHORT).show() }
    }*/
}