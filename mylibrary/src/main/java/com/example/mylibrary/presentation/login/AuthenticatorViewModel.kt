package com.example.mylibrary.presentation.login

import android.util.Base64
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.echelon.dagga.presentation.login.Encryption
import com.example.mylibrary.Constants
import com.example.mylibrary.SingleLiveData
import com.example.mylibrary.data.AuthPreferencesHelper
import com.example.mylibrary.data.NetworkResponse
import com.example.mylibrary.domain.AuthenticationInteractor
import com.example.mylibrary.isTextLengthLessThan
import com.example.mylibrary.isValidEmail
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AuthenticatorViewModel @Inject constructor(val authenticationInteractor: AuthenticationInteractor, val authPreferencesHelper: AuthPreferencesHelper): ViewModel() {

    val email = MutableLiveData<String>("")
    val shouldDisplayEmailError = MutableLiveData<Boolean>()
    val password = MutableLiveData<String>("")
    val shouldDisplayPasswordError = MutableLiveData<Boolean>()
    val token = MutableLiveData<String>()
    var hasClickedOnLoginAlready = false
    val message = SingleLiveData<Int>()
    val processInBackground = MutableLiveData<Boolean>(false)

    fun onEmailTextChanged(text: CharSequence?,
                           start: Int,
                           before: Int,
                           count: Int) {
        val email = text.toString()
        shouldDisplayEmailError.value = if(hasClickedOnLoginAlready) email.isValidEmail().not() else false
    }

    fun onPasswordTextChanged(text: CharSequence?,
                              start: Int,
                              before: Int,
                              count: Int) {
        val password = text.toString()

        shouldDisplayPasswordError.value = if(hasClickedOnLoginAlready) password.isTextLengthLessThan(6) else false
    }

    fun login(){
        hasClickedOnLoginAlready = true
        Log.e("UserEmail", email.value.toString())
        shouldDisplayEmailError.value = if(hasClickedOnLoginAlready) email.value!!.isValidEmail().not() else false
        Log.e("UserPassword", password.value.toString())
        shouldDisplayPasswordError.value = if(hasClickedOnLoginAlready) password.value!!.isTextLengthLessThan(6) else false
        if(shouldDisplayEmailError.value == false && shouldDisplayPasswordError.value == false){
            viewModelScope.launch {
                processInBackground.postValue(true)
                val response = authenticationInteractor.authenticateUser(email.value.toString(), password.value.toString())

                when(response){
                    is NetworkResponse.ApiError -> {
                        Log.e("NETWORK_RESPONSE", "API ERROR")
                        val authDataError = response.body
                        val errorCode = response.code
                        if(authDataError != null)
                            message.postValue(API_EEROR_EVENT)
                        when (errorCode) {
                            500 -> {}
                        }
                    }
                    is NetworkResponse.NetworkError -> {
                        Log.e("NETWORK_RESPONSE", "NETWORK ERROR")
                        message.postValue(NETWORK_ERROR_EVENT)
                    }
                    is NetworkResponse.Success -> {
                        Log.e("NETWORK_RESPONSE", "SUCCESS")
                        val authDataIn = response.body
                        val headers = response.headers
                        if(authDataIn.access_token != null){
                            Log.e("Token", authDataIn.access_token)
                            val map = Encryption().encrypt(authDataIn.access_token.toByteArray() , Constants.ENCRYPTION_PASSWORD.toCharArray())
                            val valueBase64String = Base64.encodeToString(map["encrypted"], Base64.NO_WRAP)
                            val saltBase64String = Base64.encodeToString(map["salt"], Base64.NO_WRAP)
                            val ivBase64String = Base64.encodeToString(map["iv"], Base64.NO_WRAP)

                            authPreferencesHelper.updateToken(valueBase64String)
                            authPreferencesHelper.updateLSalt(saltBase64String)
                            authPreferencesHelper.updateLIv(authDataIn.access_token)
                            /*viewModelScope.launch {
                                authPreferencesHelper.getToken().collect { it ->
                                    Log.e("TOKEN", it)
                                }
                            }*/
                            token.postValue(authDataIn.access_token)
                            message.postValue(SUCCESSFUL_LOGIN_EVENT)
                        }
                        else{
                            token.postValue("fchhfdffcgfddgvvdefvchgdfdgfdgcf")
                            message.postValue(SUCCESSFUL_LOGIN_EVENT)
                        }
                    }
                    is NetworkResponse.UnknownError -> {
                        Log.e("NETWORK_RESPONSE", "UNKNOWN ERROR")
                        message.postValue(UNKNOWN_ERROR_EVENT)
                    }
                }
                processInBackground.postValue(false)
            }
        }
        else{
        }
    }

    fun getToken(): String{
        return token.value!!
    }

    companion object {
        val SUCCESSFUL_LOGIN_EVENT = 1
        val UNSUCCESSFUL_LOGIN_EVENT = 2
        val UNKNOWN_ERROR_EVENT = 3
        val API_EEROR_EVENT = 4
        val NETWORK_ERROR_EVENT = 5
    }
}