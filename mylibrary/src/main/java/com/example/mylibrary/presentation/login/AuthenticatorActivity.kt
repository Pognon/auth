package com.example.mylibrary.presentation.login

import android.accounts.Account
import android.accounts.AccountAuthenticatorResponse
import android.accounts.AccountManager
import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.PorterDuff
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.example.mylibrary.Constants
import com.example.mylibrary.R
import com.example.mylibrary.data.AccountGeneral
import com.example.mylibrary.databinding.ActivityAuthenticatorBinding
import com.example.mylibrary.databinding.ActivitySignUpBinding
import dagger.hilt.android.AndroidEntryPoint
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import javax.inject.Inject

@AndroidEntryPoint
class AuthenticatorActivity : AppCompatActivity() {
    private var authenticatorResponse: AccountAuthenticatorResponse? = null
    private var resultBundle: Bundle? = null

    private val REQ_SIGNUP = 1
    private val TAG = this.javaClass.simpleName
    private var mAccountManager: AccountManager? = null
    private var mAuthTokenType: String? = null

    val viewModel: AuthenticatorViewModel by viewModels()

    /*@Inject
    lateinit var authenticationInteractor: AuthenticationInteractor*/
    lateinit var binding: ActivityAuthenticatorBinding
    lateinit var singnUpActivityBinding: ActivitySignUpBinding

    /**
     * Called when the activity is first created.
     */
    @SuppressLint("MissingSuperCall")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        authenticatorResponse =
            intent.getParcelableExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE);
        if (authenticatorResponse != null) {
            authenticatorResponse!!.onRequestContinued();
        }
        binding = ActivityAuthenticatorBinding.inflate(layoutInflater)
        singnUpActivityBinding = ActivitySignUpBinding.inflate(layoutInflater)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        mAccountManager = AccountManager.get(baseContext)
        val accountEmail = intent.getStringExtra(ARG_ACCOUNT_EMAIL)
        mAuthTokenType = intent.getStringExtra(ARG_AUTH_TYPE)
        if (mAuthTokenType == null) mAuthTokenType = AccountGeneral.AUTHTOKEN_TYPE_FULL_ACCESS
        if (accountEmail != null) {
            (binding.edtUsername as TextView).text = accountEmail
        }


        binding.btnLogin.setOnClickListener {
            viewModel.login()
        }
        binding.tvHaventAccount.setOnClickListener {
            // Since there can only be one AuthenticatorActivity, we call the sign up activity, get his results,
            // and return them in setAccountAuthenticatorResult(). See finishLogin().
            val signup = Intent(baseContext, SignUpActivity::class.java)
            intent.extras?.let { it1 -> signup.putExtras(it1) }
            startActivityForResult(signup, REQ_SIGNUP)
        }


        viewModel.shouldDisplayEmailError.observe(this) {
            if (it == true)
                binding.tvUsernameError.visibility = View.VISIBLE
            else
                binding.tvUsernameError.visibility = View.GONE

        }

        viewModel.shouldDisplayPasswordError.observe(this) {
            if (it == true)
                binding.tvPasswordError.visibility = View.VISIBLE
            else
                binding.tvPasswordError.visibility = View.GONE
        }

        viewModel.processInBackground.observe(this) {
            if (it == true) {
                binding.btnLogin.visibility = View.GONE
                binding.prbLoginProcess.apply {
                    visibility = View.VISIBLE
                    indeterminateDrawable.setColorFilter(
                        ContextCompat.getColor(
                            context,
                            R.color.white
                        ), PorterDuff.Mode.SRC_IN
                    )
                }
            } else {
                binding.btnLogin.visibility = View.VISIBLE
                binding.prbLoginProcess.visibility = View.GONE
            }
        }
        viewModel.message.observe(this) { it ->
            when (it) {
                AuthenticatorViewModel.SUCCESSFUL_LOGIN_EVENT -> {
                    Log.e("MESSAGE", "yes")
                    val token = viewModel.getToken()
                    submit(token)
                }
                AuthenticatorViewModel.UNSUCCESSFUL_LOGIN_EVENT -> {
                    displayToastErrorMessage("Identifiants incorrects")
                }
                AuthenticatorViewModel.API_EEROR_EVENT -> {
                    displayToastErrorMessage("Le serveur est down")
                }
                AuthenticatorViewModel.UNKNOWN_ERROR_EVENT -> {
                    Toast.makeText(this, "Une erreur est survenue", Toast.LENGTH_SHORT).show()
                }
                AuthenticatorViewModel.NETWORK_ERROR_EVENT -> {
                    displayToastErrorMessage("Problème lié au réseau.")
                }
            }
        }

        setContentView(binding.root)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
// The sign up activity returned that the user has successfully created an account
        if (requestCode == REQ_SIGNUP && resultCode == RESULT_OK) {
            finishLogin(data!!){
                val intent = Intent(this, Constants.activity)
                startActivity(intent)
            }
        } else super.onActivityResult(requestCode, resultCode, data)
    }

    fun submit(authToken: String) {
        Log.e("SUBMIT", "yes")
        val userEmail: String = binding.edtUsername.text.toString()
        val userPass: String = singnUpActivityBinding.edtPassword.text.toString()
        val accountType = intent.getStringExtra(ARG_ACCOUNT_TYPE)

        val executor: ExecutorService = Executors.newSingleThreadExecutor()
        val handler = Handler(Looper.getMainLooper())

        executor.execute {
            Log.d("Dagga", "$TAG> Started authenticating")
            val data = Bundle()
            try {
                data.putString(AccountManager.KEY_ACCOUNT_NAME, userEmail)
                data.putString(AccountManager.KEY_ACCOUNT_TYPE, accountType)
                data.putString(AccountManager.KEY_AUTHTOKEN, authToken)
                data.putString(PARAM_USER_PASS, userPass)

            } catch (e: Exception) {
                data.putString(KEY_ERROR_MESSAGE, e.message)
            }
            val res = Intent()
            res.putExtras(data)
            handler.post {
                if (res.hasExtra(KEY_ERROR_MESSAGE)) {
                    Toast.makeText(
                        baseContext,
                        res.getStringExtra(KEY_ERROR_MESSAGE),
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    finishLogin(res){
                        Log.e("FINISH", "yes")
                        val intent = Intent(this, Constants.activity)
                        startActivity(intent)
                    }
                }
            }
        }
    }

    private fun finishLogin(intent: Intent, afterLogin: (()->Unit)?=null) {
        Log.d("Dagga", "$TAG> finishLogin")
        val accountName: String? = intent.getStringExtra(AccountManager.KEY_ACCOUNT_NAME)
        val accountPassword: String? = intent.getStringExtra(PARAM_USER_PASS)
        val account = Account(accountName, intent.getStringExtra(AccountManager.KEY_ACCOUNT_TYPE))
        if (getIntent().getBooleanExtra(ARG_IS_ADDING_NEW_ACCOUNT, false)) {
            Log.d("Dagga", "$TAG> finishLogin > addAccountExplicitly")
            val authtoken: String? = intent.getStringExtra(AccountManager.KEY_AUTHTOKEN)
            val authtokenType = mAuthTokenType

            // Creating the account on the device and setting the auth token we got
            // (Not setting the auth token will cause another call to the server to authenticate the user)
            mAccountManager?.addAccountExplicitly(account, accountPassword, null)
            mAccountManager?.setAuthToken(account, authtokenType, authtoken)
        } else {
            Log.d("Dagga", "$TAG> finishLogin > setPassword")
            mAccountManager?.setPassword(account, accountPassword)
        }
        intent.extras?.let { setAccountAuthenticatorResult(it) }
        setResult(RESULT_OK, intent)
        finish()
        afterLogin?.invoke()
    }

    fun setAccountAuthenticatorResult(result: Bundle) {
        resultBundle = result
    }


    override fun finish() {
        if (authenticatorResponse != null) {
            if (resultBundle != null) {
                authenticatorResponse!!.onResult(resultBundle)
            } else {
                authenticatorResponse!!.onError(AccountManager.ERROR_CODE_CANCELED, "canceled")
            }
            authenticatorResponse = null
        }
        super.finish()
    }

    private fun displayToastErrorMessage(message: String) {
        val toast = Toast(applicationContext)
        toast.setGravity(Gravity.TOP + Gravity.END, 0, 0)
        toast.duration = Toast.LENGTH_LONG

        //inflate view
        val customView: View = layoutInflater.inflate(R.layout.snackbar_toast_floating_red, null)
        customView.findViewById<TextView>(R.id.toast_message).text = message
        toast.view = customView
        toast.show()
    }

    companion object {
        const val ARG_ACCOUNT_TYPE = "ACCOUNT_TYPE"
        const val ARG_AUTH_TYPE = "AUTH_TYPE"
        const val ARG_ACCOUNT_EMAIL = "ACCOUNT_EMAIL"
        const val ARG_IS_ADDING_NEW_ACCOUNT = "IS_ADDING_ACCOUNT"
        const val KEY_ERROR_MESSAGE = "ERR_MSG"
        const val PARAM_USER_PASS = "USER_PASS"
    }
}
