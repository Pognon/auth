package com.example.mylibrary.presentation.login

import android.accounts.AccountManager
import android.accounts.AccountManager.KEY_ERROR_MESSAGE
import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.PorterDuff
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.example.mylibrary.R
import com.example.mylibrary.databinding.ActivitySignUpBinding
import com.example.mylibrary.presentation.login.AuthenticatorActivity.Companion.ARG_ACCOUNT_TYPE
import com.example.mylibrary.presentation.login.AuthenticatorActivity.Companion.PARAM_USER_PASS
import dagger.hilt.android.AndroidEntryPoint
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

@AndroidEntryPoint
class SignUpActivity : AppCompatActivity() {
    private val TAG = javaClass.simpleName
    private var mAccountType: String? = null

    val viewModel: SignUpViewModel by viewModels()

    lateinit var binding: ActivitySignUpBinding
    @SuppressLint("MissingSuperCall")
    override fun onCreate(savedInstanceState: Bundle?) {
        binding = ActivitySignUpBinding.inflate(layoutInflater)
        super.onCreate(savedInstanceState)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        mAccountType = intent.getStringExtra(ARG_ACCOUNT_TYPE)
        binding.tvAlreadyHaveAccount.setOnClickListener{
                setResult(RESULT_CANCELED)
                finish()
        }
        binding.btnSignUp.setOnClickListener {
                viewModel.createAccountOnServer()
        }

        viewModel.shouldDisplayEmailError.observe(this){
            if(it == true)
                binding.tvUsernameError.visibility = View.VISIBLE
            else
                binding.tvUsernameError.visibility = View.GONE

        }

        viewModel.shouldDisplayPasswordError.observe(this){
            if(it == true)
                binding.tvPasswordError.visibility = View.VISIBLE
            else
                binding.tvPasswordError.visibility = View.GONE
        }

        viewModel.processInBackground.observe(this){
            if (it == true){
                binding.btnSignUp.visibility = View.GONE
                binding.prbLoginProcess.apply {
                    visibility = View.VISIBLE
                    indeterminateDrawable.setColorFilter(ContextCompat.getColor(context, R.color.white), PorterDuff.Mode.SRC_IN )
                }
            }
            else{
                binding.btnSignUp.visibility = View.VISIBLE
                binding.prbLoginProcess.visibility = View.GONE
            }
        }
        viewModel.message.observe(this){ it ->
            when(it){
                SignUpViewModel.SUCCESSFUL_LOGIN_EVENT -> {
                    val token = viewModel.getToken()
                    createAccount(token)
                }
                SignUpViewModel.UNSUCCESSFUL_LOGIN_EVENT -> {
                    displayToastErrorMessage("Identifiants incorrects")
                }
                SignUpViewModel.API_EEROR_EVENT ->{
                    displayToastErrorMessage("Le serveur est down")
                }
                SignUpViewModel.UNKNOWN_ERROR_EVENT -> {
                    Toast.makeText(this, "Une erreur est survenue", Toast.LENGTH_SHORT).show()
                }
                SignUpViewModel.NETWORK_ERROR_EVENT -> {
                    displayToastErrorMessage("Problème lié au réseau.")
                }
            }
        }
        setContentView(binding.root)

    }

    private fun createAccount(token: String) {
        val executor: ExecutorService = Executors.newSingleThreadExecutor()
        val handler = Handler(Looper.getMainLooper())

        executor.execute {
            var email: String = (binding.edtUsername as TextView).text.toString().trim()
            var accountPassword: String =
                binding.edtPassword.text.toString().trim()
            Log.d("Dagga", "$TAG> Started authenticating")
            val data = Bundle()
            try {
                data.putString(AccountManager.KEY_ACCOUNT_NAME, email)
                data.putString(AccountManager.KEY_ACCOUNT_TYPE, mAccountType)
                data.putString(AccountManager.KEY_AUTHTOKEN, token)
                data.putString(PARAM_USER_PASS, accountPassword)
            } catch (e: Exception) {
                data.putString(KEY_ERROR_MESSAGE, e.message)
            }
            val res = Intent()
            res.putExtras(data)
            handler.post {
                if (res.hasExtra(KEY_ERROR_MESSAGE)) {
                    Toast.makeText(
                        baseContext,
                        res.getStringExtra(KEY_ERROR_MESSAGE),
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    setResult(RESULT_OK, res)
                    finish()
                }
            }
        }
    }

    private fun displayToastErrorMessage(message: String) {
        val toast = Toast(applicationContext)
        toast.setGravity(Gravity.TOP + Gravity.END, 0, 0)
        toast.duration = Toast.LENGTH_LONG

        //inflate view
        val customView: View = layoutInflater.inflate(R.layout.snackbar_toast_floating_red, null)
        customView.findViewById<TextView>(R.id.toast_message).text = message
        toast.view = customView
        toast.show()
    }

    override fun onBackPressed() {
        setResult(RESULT_CANCELED)
        super.onBackPressed()
    }
}