package com.example.mylibrary

import android.accounts.*
import android.accounts.AccountManager.KEY_BOOLEAN_RESULT
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import com.example.mylibrary.data.AccountGeneral
import com.example.mylibrary.data.AccountGeneral.AUTHTOKEN_TYPE_FULL_ACCESS
import com.example.mylibrary.data.AccountGeneral.AUTHTOKEN_TYPE_FULL_ACCESS_LABEL
import com.example.mylibrary.data.AccountGeneral.AUTHTOKEN_TYPE_READ_ONLY
import com.example.mylibrary.data.AccountGeneral.AUTHTOKEN_TYPE_READ_ONLY_LABEL
import com.example.mylibrary.presentation.login.AuthenticatorActivity


class DaggaAuthenticator(val context: Context) : AbstractAccountAuthenticator(context) {
    private val TAG = "DaggaAuthenticator"
    @Throws(NetworkErrorException::class)
    override fun addAccount(
        response: AccountAuthenticatorResponse?,
        accountType: String?,
        authTokenType: String?,
        requiredFeatures: Array<String?>?,
        options: Bundle?
    ): Bundle {
        Log.d("Dagga", "$TAG> addAccount")
        val intent = Intent(context, AuthenticatorActivity::class.java)
        intent.putExtra(AuthenticatorActivity.ARG_ACCOUNT_TYPE, accountType)
        intent.putExtra(AuthenticatorActivity.ARG_AUTH_TYPE, authTokenType)
        intent.putExtra(AuthenticatorActivity.ARG_IS_ADDING_NEW_ACCOUNT, true)
        intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response)
        val bundle = Bundle()
        bundle.putParcelable(AccountManager.KEY_INTENT, intent)
        return bundle
    }

    @Throws(NetworkErrorException::class)
    override fun getAuthToken(
        response: AccountAuthenticatorResponse?,
        account: Account,
        authTokenType: String,
        options: Bundle?
    ): Bundle {
        Log.d("Dagga", "$TAG> getAuthToken")

        // If the caller requested an authToken type we don't support, then
        // return an error
        if (authTokenType != AccountGeneral.AUTHTOKEN_TYPE_READ_ONLY && authTokenType != AccountGeneral.AUTHTOKEN_TYPE_FULL_ACCESS) {
            val result = Bundle()
            result.putString(AccountManager.KEY_ERROR_MESSAGE, "invalid authTokenType")
            return result
        }

        // Extract the username and password from the Account Manager, and ask
        // the server for an appropriate AuthToken.
        val am: AccountManager = AccountManager.get(context)
        var authToken: String = am.peekAuthToken(account, authTokenType)
        Log.d("Dagga", "$TAG> peekAuthToken returned - $authToken")

        // Lets give another try to authenticate the user
        if (TextUtils.isEmpty(authToken)) {
            val password: String = am.getPassword(account)
            if (password != null) {
                try {
                    Log.d("Dagga", "$TAG> re-authenticating with the existing password")
                    /*authToken = sServerAuthenticate.userSignIn(account.name, password, authTokenType)*/
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }

        // If we get an authToken - we return it
        if (!TextUtils.isEmpty(authToken)) {
            val result = Bundle()
            result.putString(AccountManager.KEY_ACCOUNT_NAME, account.name)
            result.putString(AccountManager.KEY_ACCOUNT_TYPE, account.type)
            result.putString(AccountManager.KEY_AUTHTOKEN, authToken)
            return result
        }

        // If we get here, then we couldn't access the user's password - so we
        // need to re-prompt them for their credentials. We do that by creating
        // an intent to display our AuthenticatorActivity.
        val intent = Intent(context, AuthenticatorActivity::class.java)
        intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response)
        intent.putExtra(AuthenticatorActivity.ARG_ACCOUNT_TYPE, account.type)
        intent.putExtra(AuthenticatorActivity.ARG_AUTH_TYPE, authTokenType)
        intent.putExtra(AuthenticatorActivity.ARG_ACCOUNT_EMAIL, account.name)
        val bundle = Bundle()
        bundle.putParcelable(AccountManager.KEY_INTENT, intent)
        return bundle
    }

    override fun getAuthTokenLabel(authTokenType: String): String {
        return if (AUTHTOKEN_TYPE_FULL_ACCESS.equals(authTokenType)) AUTHTOKEN_TYPE_FULL_ACCESS_LABEL else if (AUTHTOKEN_TYPE_READ_ONLY.equals(
                authTokenType
            )
        ) AUTHTOKEN_TYPE_READ_ONLY_LABEL else "$authTokenType (Label)"
    }

    @Throws(NetworkErrorException::class)
    override fun hasFeatures(
        response: AccountAuthenticatorResponse?,
        account: Account?,
        features: Array<String?>?
    ): Bundle {
        val result = Bundle()
        result.putBoolean(KEY_BOOLEAN_RESULT, false)
        return result
    }

    override fun editProperties(response: AccountAuthenticatorResponse?, accountType: String?): Bundle? {
        return null
    }

    @Throws(NetworkErrorException::class)
    override fun confirmCredentials(
        response: AccountAuthenticatorResponse?,
        account: Account?,
        options: Bundle?
    ): Bundle? {
        return null
    }

    @Throws(NetworkErrorException::class)
    override fun updateCredentials(
        response: AccountAuthenticatorResponse?,
        account: Account?,
        authTokenType: String?,
        options: Bundle?
    ): Bundle? {
        return null
    }

}