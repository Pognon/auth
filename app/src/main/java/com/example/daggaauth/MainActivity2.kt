package com.example.daggaauth

import android.accounts.AccountManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.mylibrary.DaggAuth
import com.example.mylibrary.data.AccountGeneral

class MainActivity2 : AppCompatActivity() {
    private var mAccountManager: AccountManager? = null
    lateinit var daggAuth: DaggAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        mAccountManager = AccountManager.get(this)
        super.onCreate(savedInstanceState)
        daggAuth = DaggAuth()
        daggAuth.setActivity(MainActivity::class.java)
        daggAuth.getTokenForAccountCreateIfNeeded(this, mAccountManager, AccountGeneral.ACCOUNT_TYPE, AccountGeneral.AUTHTOKEN_TYPE_FULL_ACCESS)
        //setContentView(R.layout.activity_main2)
    }
}